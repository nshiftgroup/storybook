## Webshipper Storybook independent application and component library

[Storybook](https://storybook.js.org/docs/react/get-started/introduction) is a Component Driven Design guidelines and structural application that allows the developer to quickly change, test or develop (atomic design) components, like a button and bigger components like a header with a dependency on small components like a button.

Storybook has a folder named stories with subfolders containing groups of same components ie. buttons/

Stories co-exists with their component ie `Button.vue` has a `Button.stories.js`

WS Storybook runs on Vue 2.6.11

### Storybook setup

in the root directory run

```
yarn build-storybook
```

When storybook has been build run it through

```
yarn storybook
```

Storybook should print to the terminal the localhost address of your storybook service. Go to that.

Discover the UI and all the niceties it offers. Develop nice things. (Storybook hot-reloads)

### Publish stories

When youve made a component with a story, or updated one, update package.json in storybook to next small increment (or larger if big changes)

Then simply follow normal guidelines; make a PR to develop, then main.

If you want to update the project USING storybook, you need to rebuild that project updating packages with `Yarn` - Yarn will pull the latest from the storybook bitbucket repository then and update yarn.lock. This file needs to be commited to your build branch (develop, master etc)

### Setup storybook on a desired project

Manually add to package.json on your project like this

```
 "@webshipper/vue-components": "git+ssh://git@bitbucket.org/webshiprdev/storybook.git#master",
```

Make a new SSH key somewhere

```
ssh-keygen -t rsa -b 4096 -N '' -f my_ssh_key
```

copy the private key and make a access key here

https://bitbucket.org/webshiprdev/storybook/admin/access-keys/

Add the public SSH key to the project that needs access to storybook as a repository variable and use it in the build and docker as on webshipper frontend.

### Use components externally

Use in components like

script:

```
// Vue
import WsActionButton from '@webshipper/vue-components/src/components/buttons/action-button.vue';

//SCSS
@import "@webshipper/vue-components/src/components/styles/variables.scss";

```

html:

```
<my-button primary size="small"
@onClick="onCreateAccount"
label="Sign up" v-if="!user" />
```

### Work on storybook components/stories and test simultaneously/instantly/locally in separated vue application

in storybook root folder

run

```
npm link
```

in the node_modules folder of webshipr-frontend or webshipr-backend run:

```
npm link @webshipper/vue-components
```

check that the `node_module/@webshipper` is symlinked to your local directory

check that `package.json` still points to version number of dependency and NOT local version (this can happen with `npm i ../storybook/..` )

develop nice things!

#### Development guidelines

- Read this https://storybook.js.org/docs/react/writing-stories/introduction

- You can add a snippet through visualcode using `sbase` keyword, and properties with `s` + .. bool, etc, autocomplete should show the available premade properties

- All components must have a story. The story should be in a relevant path in the GUI. ie; `title: 'WS Design System/Atoms/Input/Inputfield styled dropdown button without input, with hanging label',`

- Give stories a fully proper detailed description.

- Stories are coupled to a component in a separate folder containing story and vue component. Named in the same way, story is "stories.js", ie. Button.vue, Button.stories.js

- Stories are written in JS format (mdx support is not good enough for vue yet)

- Stories should be as descriptive as possibly makes sense. All properties should have a description

#### Small hacks and nice things:

Show all your npm links

```
npm ls -g --depth=0 --link=true
```

#### Bugs, findings and other things:

While Storybook and MDX "supports" Vuejs, it is in an alpha and buggy state that causes more headache than the current output is worth.

#### Webshipr FE necessary changes

Webpack.config.js needs these changes for using components from node_modules ->

```
include: [/node_modules\/sweet-modal-src/, /node_modules\/vue-flag-icon/, /node_
modules\/@webshipper/, /node_modules\/gmap-vue/, /src/], // Some modules are messed up and need to be compiles with vue loader
```
