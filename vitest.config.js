import { resolve } from 'path'
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue2';

// https://vitejs.dev/config/
export default defineConfig({
  root: '.',
  plugins: [vue()],
  test: {
    globals: true,
    environment: 'jsdom'
  },
  resolve: {
    alias: {
      '~': resolve(__dirname, './'),
      '@': resolve(__dirname, './'),
      '~/': resolve(__dirname, './'),
      '@/': resolve(__dirname, './'),
      '~~/': resolve(__dirname, './'),
      '@@/': resolve(__dirname, './'),
      '@store/': resolve(__dirname, './store/')
    }
  }
})
