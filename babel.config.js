module.exports = {
  presets: ['@vue/cli-plugin-babel/preset'],
  plugins: [
    //Gets stuck on build without this
    ['@babel/plugin-proposal-private-property-in-object', { loose: true }]
  ]
};
