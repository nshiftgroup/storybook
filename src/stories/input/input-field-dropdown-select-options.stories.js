import WsInputfieldDropdownSelectOptions from '../../components/input/input-field-dropdown-select-options.vue';

export default {
  title: 'WS Design System/Atoms/Input/Inputfield styled dropdown button without input, with hanging label',
  component: WsInputfieldDropdownSelectOptions,
  parameters: {
    docs: {
      description: {
        component:
          'This component does not take a input other than the dropdown, its basically a dropdown with input styling. Takes select and options'
      }
    }
  },
  argTypes: {
    withSelect: {
      description: 'adds a select element into the component and slots inside that',
      defaultValue: false
    },

    label: {
      description: 'The label that is shown. Shown in middle when no value, and shown on top when value exists.',
      control: { type: 'text' },
      defaultValue: 'placeholder'
    },
    isLoading: {
      description: 'Replaces dropdown arrow with loading spinner when isLoading is true',
      control: 'boolean',
      defaultValue: false
    },
    customClass: {
      description: 'Add a custom class to the input field.'
    },
    disabled: {
      description: 'Disabled the dropdown when set to true',
      control: 'boolean',
      defaultValue: false
    },
    smallPadding: {
      description: 'Halves the padding to give more room inside input field.',
      control: 'boolean',
      defaultValue: false
    }
  }
};

const DropdownTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsInputfieldDropdownSelectOptions },
  template:
    `<WsInputfieldDropdownSelectOptions v-bind='$props'>` +
    `<template>${args.slot_content}</template>` +
    `</WsInputfieldDropdownSelectOptions>`
});

export const Dropdown = DropdownTemplate.bind({});
Dropdown.args = {
  slot_content: `<select><option>Option 1</option> <option>Option 2</option></select>`
};

export const LoadingContent = DropdownTemplate.bind({});
LoadingContent.args = {
  slot_content: `<select></select>`,
  isLoading: true
};

export const Disabled = DropdownTemplate.bind({});
Disabled.args = {
  slot_content: `<select><option>Option 1</option> <option>Option 2</option></select>`,
  disabled: true
};
