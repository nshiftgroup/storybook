/**
 * ArgTypes proptypes below. Enjoy!
 * sbool
 * snumber
 * srange
 * stext
 * scolor
 * sselect
 * smulti
 **/
import WsCheckbox from '../../components/input/checkbox.vue';

export default {
  title: 'WS Design System/Atoms/Checkbox',
  component: WsCheckbox,
  parameters: {
    docs: {
      description: {
        component: 'A checkbox that acts like a normal input type="checkbox"'
      }
    }
  },
  argTypes: {
    disabled: {
      description: 'Disables the component if set to true',
      control: 'boolean',
      defaultValue: false
    },
    label: {
      description: 'Label displayed next to checkbox',
      control: 'text',
      defaultValue: 'Checkbox'
    },
    falseValue: {
      description: 'Value that will be returned when not checked, default is false, but can be any value',
      control: 'text',
      defaultValue: false
    },
    trueValue: {
      description: 'Value that will be returned when checked, default is true, but can be any value',
      control: 'text',
      defaultValue: true
    }
  }
};

const vModel = false;

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: () => ({ vModel }),
  components: { WsCheckbox },
  template: '<WsCheckbox v-bind="$props" v-model="vModel" />'
});

const SlotTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsCheckbox },
  template: `<WsCheckbox v-bind='$props'>` + `<template>${args.slot_content}</template>` + `</WsCheckbox>`
});

export const Primary = Template.bind({});
Primary.args = {};

export const SlotLabel = SlotTemplate.bind({});
SlotLabel.args = {
  dropdown: true,
  label: undefined,
  slot_content: `<div>Label in slot</div>`
};
