/**
* ArgTypes proptypes below. Enjoy!
* sbool
* snumber
* srange
* stext
* scolor
* sselect
* smulti
**/
import WsRangeSlider from '../../components/input/range-slider.vue'

export default {
  title: 'WS Design System/Atoms/Input/Range Slider',
  component: WsRangeSlider,
  parameters: {
    docs: {
      description: {
        component: 'Slider that works exactly like a normal html input type="slider", but with a Webshipper theme'
      }
    }
  },
  argTypes: {
    min: {
      description: 'Minimum value for slider',
      control: 'number',
      defaultValue: 0
    },
    max: {
      description: 'Max value for slider',
      control: 'number',
      defaultValue: 100
    },
    step: {
      description: 'How much should be incremented when moving slider?',
      control: 'number',
      defaultValue: 1
    }
  }
};

let vModel = '';

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: () => ({ vModel }),
  components: { WsRangeSlider },
  template:
   "<WsRangeSlider v-bind='$props' v-model='vModel' />"
});


export const Primary = Template.bind({});
Primary.args = {
  
};
export const FourSteps = Template.bind({});
FourSteps.args = {
  min: 0,
  max: 3
};
