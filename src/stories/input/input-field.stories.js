import WsInputField from '../../components/input/input-field.vue';

export default {
  title: 'WS Design System/Atoms/Input/Input Field',
  component: WsInputField,
  parameters: {
    docs: {
      description: {
        component: 'An input field that has a placeholder text, which will transition to top on user interaction.'
      }
    }
  },
  argTypes: {
    value: {
      description: 'The value that is changed when user types in this field',
      control: { type: 'object' }
    },
    label: {
      description: 'The label that is shown. Shown in middle when no value, and shown on top when value exists.',
      control: { type: 'text' },
      defaultValue: 'placeholder'
    },
    customclass: {
      description: 'Add a custom class to the input field.'
    },
    requiredInput: {
      description: 'Toggles a red "required"-field underneath text-field',
      control: 'boolean',
      defaultValue: false
    },
    disabled: {
      description: 'Disables input with the input field',
      control: 'boolean',
      defaultValue: false
    },
    smallPadding: {
      description: 'Halves the padding to give more room inside input field.',
      control: 'boolean',
      defaultValue: false
    },
    maxLength: {
      description: 'Sets the maximum number of characters the user can input',
      control: 'number'
    },
    type: {
      description:
        'Allows to set the type of the input field to password or text, password will iniate browsers default password behavior',
      control: 'text',
      defaultValue: 'text'
    },
    invalidInput: {
      description: 'Changes color of focus bar at bottom to red to indicate invalid input',
      control: 'boolean',
      defaultValue: false
    },
    validateAsNumber: {
      description: 'Will function like a type="number" input-field, but allows for both commas and dots',
      control: 'boolean',
      defaultValue: false
    },
    disableAutoFill: {
      description: 'If set to true, chrome will not auto-fill password',
      control: 'boolean',
      defaultValue: false
    },
    showButton: {
      description: 'If set to true, a button will be shown on the far-right side',
      control: 'boolean',
      defaultValue: false
    },
    requiredText: {
      description: 'Text to show when input is required',
      control: 'text',
      defaultValue: 'Required'
    }
  }
};

let vModel = '';

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: () => ({ vModel }),
  components: { WsInputField },
  template: "<WsInputField v-bind='$props' v-model='vModel' />"
});

export const Default = Template.bind({});
Default.args = {};

export const WithValue = Template.bind({});
WithValue.args = {
  vModel: 'This is a value'
};

export const Required = Template.bind({});
Required.args = {
  requiredInput: true,
  invalidInput: true
};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true
};

export const Password = Template.bind({});
Password.args = {
  type: 'password'
};

export const Numbdf = Template.bind({});
Number.args = {
  validateAsNumber: true
};
