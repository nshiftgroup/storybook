import WsRadioButton from '../../components/input/radio-button.vue';

export default {
  title: 'WS Design System/Atoms/Input/Radio button',
  component: WsRadioButton,
  parameters: {
    docs: {
      description: {
        component:
          'A single generic radio button with a slot that can be used instead of ' +
          'the :label, e.g. to display an icon.'
      }
    }
  },
  argTypes: {
    label: {
      description: 'Text shown next to the radio button',
      control: 'text',
      defaultValue: 'label text'
    },
    showLabel: {
      description: 'Boolean to toggle whether to show label',
      control: 'boolean',
      defaultValue: true
    },
    inputValue: {
      description: 'This is the value v-model will set to when the radio button is checked.',
      control: 'object'
    },
    value: {
      description: 'Used to make v-model work. Should not be used directly.',
      control: 'object'
    }
  }
};

const vModel = 'sameValue';

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: () => ({ vModel }),
  components: { WsRadioButton },
  template: '<WsRadioButton v-bind="$props" v-model="vModel" />'
});

export const Active = Template.bind({});

export const NotActive = Template.bind({});
NotActive.args = {
  inputValue: 'notSameValue'
};

export const NoLabel = Template.bind({});
NoLabel.args = {
  showLabel: false
};
