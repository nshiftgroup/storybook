import WsInputFieldDropdownBtn from '../../components/input/input-field-dropdown-btn.vue';

export default {
  title: 'WS Design System/Atoms/Input/Inputfield styled dropdown button without input',
  component: WsInputFieldDropdownBtn,
  parameters: {
    docs: {
      description: {
        component: 'A input styled dropdown button, that does not take a entered input'
      }
    }
  },
  argTypes: {
    label: {
      description: 'The label that is shown. Shown in middle when no value, and shown on top when value exists.',
      control: { type: 'text' },
      defaultValue: 'placeholder'
    },
    customClass: {
      description: 'Add a custom class to the input field.'
    },
    disabled: {
      description: 'Disables the dropdown menu',
      control: 'boolean',
      defaultValue: false
    }
  }
};

const DropdownTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsInputFieldDropdownBtn },
  template:
    `<WsInputFieldDropdownBtn v-bind='$props'>` +
    `<template>${args.slot_content}</template>` +
    `</WsInputFieldDropdownBtn>`
});

export const Dropdown = DropdownTemplate.bind({});
Dropdown.args = {
  label: 'A label',
  slot_content: `<a class="dropdown-item">Option 1</a> <a class="dropdown-item">Option 2</a>`
};

export const Disabled = DropdownTemplate.bind({});
Disabled.args = {
  label: 'A label',
  slot_content: `<a class="dropdown-item">Option 1</a> <a class="dropdown-item">Option 2</a>`,
  disabled: true
};
