import WsTextarea from '../../components/input/textarea.vue';

export default {
  title: 'WS Design System/Atoms/Input/Textarea',
  component: WsTextarea,
  parameters: {
    docs: {
      description: {
        component: 'A textarea that has a placeholder text, which will transition to top on user interaction.'
      }
    }
  },
  argTypes: {
    value: {
      description: 'The value that is changed when user types in this field',
      control: { type: 'object' }
    },
    label: {
      description:
        'The label that is shown. Shown in lowercase when no value, and shown on top and uppercase when value exists.',
      control: { type: 'text' },
      defaultValue: 'placeholder'
    },
    customClass: {
      description: 'Add a custom class to the textarea.'
    },
    disableExpand: {
      description: 'Toggles whether or not to make it possible to expand',
      control: 'boolean',
      defaultValue: false
    },
    disabled: {
      description: 'Disables the textarea from input',
      control: 'boolean',
      defaultValue: false
    },
    requiredInput: {
      description: 'Toggles a red "required"-field underneath textarea',
      control: 'boolean',
      defaultValue: false
    },
    smallPadding: {
      description: 'Halves the padding to give more room inside text area.',
      control: 'boolean',
      defaultValue: false
    },
    invalidInput: {
      description: 'Changes color of focus bar at bottom to red to indicate invalid input',
      control: 'boolean',
      defaultValue: false
    }
  }
};

let object_to_edit = {
  inner: {
    value: 'value'
  }
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsTextarea },
  template: "<WsTextarea v-bind='$props' />"
});

export const Default = Template.bind({});
Default.args = {};

export const WithValue = Template.bind({});
WithValue.args = {
  value: object_to_edit.inner.value
};

export const invalidInput = Template.bind({});
invalidInput.args = {
  invalidInput: true
};

export const Disabled = Template.bind({});
Disabled.args = {
  value: object_to_edit.inner.value,
  disabled: true
};
