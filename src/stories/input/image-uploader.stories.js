/**
 * ArgTypes proptypes below. Enjoy!
 * sbool
 * snumber
 * srange
 * stext
 * scolor
 * sselect
 * smulti
 **/
import WsImageUploader from '../../components/input/image-uploader.vue';
import { action } from '@storybook/addon-actions';

export default {
  title: 'WS Design System/Atoms/Input/Image uploader styled as a input field',
  component: WsImageUploader,
  parameters: {
    docs: {
      description: {
        component:
          'Creates a basic image uploade component, thats styled much like any other WS input. Sends the event through @input.'
      }
    }
  },
  argTypes: {
    label: {
      description: 'A label shown inside the field',
      control: { type: 'text' }
    },
    value: {
      description: 'Can take a preexisting image'
    },
    accept: {
      description: 'Defines what type to accept',
      defaultValue: 'image/*'
    },
    requiredInput: {
      description: 'Toggles a red "required"-field underneath image-uploader',
      control: 'boolean',
      defaultValue: false
    },
    requiredText: {
      description: 'Text to show when requiredInput is true',
      control: 'text',
      defaultValue: 'Required'
    },
    deleteText: {
      description: 'Text to show on delete button',
      control: 'text',
      defaultValue: 'Delete'
    },
    changeText: {
      description: 'Text to show on change button',
      control: 'text',
      defaultValue: 'Change'
    },
    dragDropText: {
      description: 'Text to show on drag and drop',
      control: 'text',
      defaultValue: 'Drag and drop image here'
    }
  }
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsImageUploader },
  template: "<WsImageUploader @input='action' v-bind='$props' />",
  methods: { action: action('clicked') }
});
export const Label = Template.bind({});
Label.args = {
  label: 'Upload a image, note there is translations fields in this component that needs to be added to vue translate'
};

export const Required = Template.bind({});
Required.args = {
  requiredInput: true,
  invalidInput: true
};
