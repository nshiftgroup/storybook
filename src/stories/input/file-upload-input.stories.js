/**
 * ArgTypes proptypes below. Enjoy!
 * sbool
 * snumber
 * srange
 * stext
 * scolor
 * sselect
 * smulti
 **/
import WsFileUploadInput from '../../components/input/file-upload-input.vue';
import { action } from '@storybook/addon-actions';

export default {
  title: 'WS Design System/Atoms/Input/File upload styled as a input field',
  component: WsFileUploadInput,
  parameters: {
    docs: {
      description: {
        component:
          'Creates a basic fileupload component, thats styled like any other WS input. Sends the even through @change, so catch that.'
      }
    }
  },
  argTypes: {
    label: {
      description: 'A label shown inside the input field',
      control: { type: 'text' },
      defaultValue: 'Pick a file'
    },
    customClass: {
      description: 'Add a custom class to the field.'
    }
  }
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsFileUploadInput },
  template: "<WsFileUploadInput @change='action' v-bind='$props' />",
  methods: { action: action('clicked') }
});
export const Label = Template.bind({});
Label.args = {
  label: 'Pick a file'
};
