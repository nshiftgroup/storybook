import WsLengthUnitPicker from '../../../components/input/pickers/length-unit-picker.vue';

export default {
  title: 'WS Design System/Pickers/Length Unit Picker',
  component: WsLengthUnitPicker,
  parameters: {
    docs: {
      description: {
        component: 'Component that uses the dropdown to display different length units.'
      }
    }
  },
  argTypes: {
    value: {
      description: 'This should not be set, use v-model instead',
      control: 'text'
    },
    disabled: {
      description: 'Disables the picker from input',
      control: 'boolean',
      defaultValue: false
    },
    smallPadding: {
      description: 'Halves the padding to give more room inside input field.',
      control: 'boolean',
      defaultValue: false
    },
    dropup: {
      description: 'Changes dropdown direction to dropup.',
      control: 'boolean',
      defaultValue: false
    }
  }
};

let model = 'm';

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: () => ({ model }),
  components: { WsLengthUnitPicker },
  template: "<WsLengthUnitPicker v-bind='$props' v-model='model' />"
});

export const Default = Template.bind({});

export const PreSelectedValue = Template.bind({});
PreSelectedValue.args = {};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true
};
