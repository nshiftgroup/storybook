import WsColorPickerWithInput from '../../../components/input/pickers/color-picker-with-input.vue';

export default {
  title: 'WS Design System/Atoms/Pickers/Color Picker With Input',
  component: WsColorPickerWithInput,
  parameters: {
    docs: {
      description: {
        component: 'Component that uses a visual color picker or input to define a HEX color.'
      }
    }
  },
  argTypes: {
    defaultValue: {
      description: 'Predefined HEX color',
      control: 'text'
    },
    label: {
      description: 'Label text for the picker',
      control: 'text'
    }
  }
};

let model = '';

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: () => ({ model }),
  components: { WsColorPickerWithInput },
  template: "<WsColorPickerWithInput v-bind='$props' v-model='model' />"
});

export const Default = Template.bind({});
Default.args = {
  label: "pick a color"
}
export const PredefinedColor = Template.bind({});
PredefinedColor.args = {
  defaultValue: "#111111",
  label: "pick a color"
};
