/**
 * ArgTypes proptypes below. Enjoy!
 * sbool
 * snumber
 * srange
 * stext
 * scolor
 * sselect
 * smulti
 **/
import WsToggleSwitch from '../../components/input/toggle-switch.vue';

export default {
  title: 'WS Design System/Atoms/input/Toggle Switch',
  component: WsToggleSwitch,
  parameters: {
    docs: {
      description: {
        component: 'Switch used to toggle a boolean.'
      }
    }
  },
  argTypes: {
    value: {
      description: 'You should just use v-model with this component',
      control: 'boolean',
      defaultValue: false
    },
    disabled: {
      description: 'Disables emitting events from the switch',
      control: 'boolean',
      defaultValue: false
    }
  }
};

let vModel = false;

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: () => ({ vModel }),
  components: { WsToggleSwitch },
  template: "<WsToggleSwitch v-bind='$props' v-model='vModel' />"
});

export const Default = Template.bind({});
Default.args = {};

export const Active = Template.bind({});
Active.args = {
  value: true
};

export const WithLabel = Template.bind({});
WithLabel.args = {
  label: 'I am a label'
};

export const LabelPositionedRight = Template.bind({});
LabelPositionedRight.args = {
  label: 'I am a label',
  labelRight: true
};
