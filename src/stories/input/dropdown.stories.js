import WsDropdown from '../../components/input/dropdown.vue';

const mock_data = [
  {
    value: 'a',
    label: 'Apples'
  },
  {
    value: 'b',
    label: 'Beans'
  },
  {
    value: 'c',
    label: 'Lemons'
  },
  {
    value: 'd',
    label: 'Almonds'
  }
];

const mock_data_mixed = [
  {
    value: 1,
    label: 'Int 1'
  },
  {
    value: '2',
    label: 'String 2'
  },
  {
    value: 3,
    label: 'Int 3'
  },
  {
    value: '4',
    label: 'String 4'
  }
];

const mock_data_objects = [
  {
    value: {
      id: 'APPLES',
      name: 'Apples'
    },
    label: 'Apples'
  },
  {
    value: {
      id: 2,
      name: 'Beans'
    },
    label: 'Beans'
  },
  {
    value: {
      id: 'LEMONS',
      name: 'Lemons'
    },
    label: 'Lemons'
  },
  {
    value: {
      id: 'ALMONDS',
      name: 'Almonds'
    },
    label: 'Almonds'
  }
];

export default {
  title: 'WS Design System/Atoms/Input/Dropdown',
  component: WsDropdown,
  parameters: {
    docs: {
      description: {
        component:
          'The most basic dropdown. Can be used in its simple form. ALSO used by all other dropdowns ' +
          'in the storybook library.'
      }
    }
  },
  argTypes: {
    options: {
      description:
        'The options that will be shown when the dropdown is active. Consists of array of object ' +
        'each containing <strong>value</strong> and <strong>label</strong>.'
    },
    placeholder: {
      description: 'Placeholder text to show in search-field inside dropdown',
      control: 'text',
      defaultValue: 'Search..'
    },
    value: {
      description:
        'The current value of the input. Used with v-model. If "multiple" is true, then value must be an array '
    },
    label: {
      description: 'Label that is shown in the middle when no value is selected',
      control: 'text',
      defaultValue: 'label'
    },
    isLoading: {
      description: 'Replaces dropdown arrow with loading spinner when isLoading is true',
      control: 'boolean',
      defaultValue: false
    },
    identifiedBy: {
      description:
        'Used when value in options is are objects, and must be set if this is the case. Enabled comparing objects by a single key, like id, instead of full objects.'
    },
    bold: {
      description: 'Sets the selection option to be rendered as bold',
      control: 'boolean',
      defaultValue: false
    },
    hide_search: {
      description: 'Toggles whether to show searchfield in dropdown menu',
      control: 'boolean',
      defaultValue: false
    },
    disabled: {
      description: 'Disables the dropdown from user input',
      control: 'boolean',
      defaultValue: false
    },
    smallPadding: {
      description: 'Halves the padding to give more room inside input field.',
      control: 'boolean',
      defaultValue: false
    },
    multiple: {
      description: 'If enabled, this component will allow selecting multiple items in a tag-style way. ',
      control: 'boolean',
      defaultValue: false
    }
  }
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsDropdown },
  template: "<WsDropdown v-bind='$props'/>"
});

export const Primary = Template.bind({});
Primary.args = {
  options: mock_data
};

export const MixedValues = Template.bind({});
MixedValues.args = {
  options: mock_data_mixed,
  value: 2
};

export const NoSearch = Template.bind({});
NoSearch.args = {
  options: mock_data,
  hide_search: true
};

export const Disabled = Template.bind({});
Disabled.args = {
  options: mock_data,
  disabled: true
};

export const Multiple = Template.bind({});
Multiple.args = {
  options: mock_data,
  multiple: true
};

export const SingleObject = Template.bind({});
SingleObject.args = {
  identifiedBy: 'id',
  value: { id: '2', otherAttr: '  ' },
  options: mock_data_objects,
  multiple: false
};

export const MultipleObjects = Template.bind({});
MultipleObjects.args = {
  identifiedBy: 'id',
  value: [{ id: 2, otherAttr: '  ' }, { id: 3 }],
  options: mock_data_objects,
  multiple: true
};
