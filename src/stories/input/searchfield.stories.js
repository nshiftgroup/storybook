import WsSearchfield from '../../components/input/searchfield.vue';

export default {
  title: 'WS Design System/Atoms/Input/Searchfield',
  component: WsSearchfield,
  parameters: {
    docs: {
      description: {
        component: 'A very basic searchfield that can be used everywhere in Webshipper.'
      }
    }
  },
  argTypes: {
    placeholder: {
      description: 'Placeholder text that will be shown inside searchfield when no text has been input',
      defaultValue: 'search'
    },
    show_icon: {
      description: 'Whether or not to show magnifying glass icon.',
      control: 'boolean',
      defaultValue: true
    },
    input: {
      description: '$emit everytime input is changed. Use v-model with it!'
    },
    canClear: {
      description: 'Toggles whether there should be displayed icon to clear content of searchfield',
      control: 'boolean',
      defaultValue: false
    },
    shape: {
      description: 'Sets the shape of the searchfield',
      control: { type: 'select', options: ['round', 'square'] }
    
    },
    noAutoSuggest: {
      description: 'Adds a attribute "autocomplete="new-password" to the input field, so browsers will not try to suggest something to autofill the input',
      control: 'boolean',
      defaultValue: false
    },
    hasDropdownFiltering: {
      description: 'Toggles a little arrow to be visible that toggles a dropdown which uses @dropdownItems',
      control: 'boolean',
      defaultValue: false
    },
    dropdownItems: {
      description: 'List containing label, value objects',
      control: { type: 'select', options: [{label: 'option 1', value: 1}, {label: 'option 2',value: 2}, {label: 'option 3',value: 3}]},
    },
    dropdownValue: {
      description: 'Value of currently selection from dropdownItems',
      control: 'text',
      defaultValue: 1
    }
  }
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsSearchfield },
  template: "<WsSearchfield v-bind='$props' />"
});

export const Default = Template.bind({});
Default.args = {};

export const NoIcon = Template.bind({});
NoIcon.args = {
  show_icon: false
};

export const Square = Template.bind({});
Square.args = {
  shape: 'square'
};

export const canClear = Template.bind({});
canClear.args = {
  canClear: true
};

export const dropdown = Template.bind({});
dropdown.args = {
  hasDropdownFiltering: true,
  dropdownItems: [{label: 'option 1', value: 1}, {label: 'option 2',value: 2}, {label: 'option 3',value: 3}]
};
