import WsNumberInputField from '../../components/input/number-input-field.vue';

export default {
  title: 'WS Design System/Atoms/Input/Number Inputfield',
  component: WsNumberInputField,
  parameters: {
    docs: {
      description: {
        component: 'An input field made for numbers with number-specific logic.'
      }
    }
  },
  argTypes: {
    value: {
      description: 'Use v-model with this component',
      control: 'number'
    },
    label: {
      description: 'Label to show inside input field',
      control: 'text'
    },
    min: {
      description: 'Set the minimum number that can be input',
      control: 'number'
    },
    max: {
      description: 'Set the maximum number that can be input',
      control: 'number'
    },
    disabled: {
      description: 'Disables user input and changes background',
      control: 'boolean',
      defaultValue: false
    },
    disableSpecialChar: {
      description: 'Disables the use of commas and other special characters',
      control: 'boolean',
      defaultValue: false
    },
    hideButtons: {
      description: 'If true, the "up" and "down" button will be hidden',
      control: 'boolean',
      defaultValue: false
    },
    smallPadding: {
      description: 'Halves the padding to give more room inside input field.',
      control: 'boolean',
      defaultValue: false
    },
    invalidInput: {
      description: 'Changes color of focus bar at bottom to red to indicate invalid input',
      control: 'boolean',
      defaultValue: false
    }
  }
};

let vModel = 0;

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: () => ({ vModel }),
  components: { WsNumberInputField },
  template: "<WsNumberInputField v-bind='$props' v-model='vModel' />"
});

export const Primary = Template.bind({});
Primary.args = {};

export const WithValue = Template.bind({});
WithValue.args = {
  vModel: 2
};

export const WithLabel = Template.bind({});
WithLabel.args = {
  label: 'This is a label'
};

export const HideButtons = Template.bind({});
HideButtons.args = {
  hideButtons: true
};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true
};

export const InvalidInput = Template.bind({});
InvalidInput.args = {
  invalidInput: true
};

export const DisableSpecialCharacters = Template.bind({});
DisableSpecialCharacters.args = {
  disableSpecialChar: true
};
