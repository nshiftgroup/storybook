import WsTabsGroup from '../../components/tabs/tabs-group.vue'
import WsTab from '../../components/tabs/tab.vue'

export default {
  title: 'WS Design System/Navigation/Tabs',
  component: WsTabsGroup,
  parameters: {
    docs: {
      description: {
        component: 'A WsTabsGroup component that is supposed to be used with up to five WsTab. WsTabsGroup is using slot and so is WsTab. ' 
        + 'Simply place WsTab inside the WsTabsGroup element. If content in the tab is not supposed to be remounted, '
        + 'use the tab-kept-alive.vue component instead. This uses v-show instead of v-if. Both has pros and cons'
      }
    },
  },
  argTypes: {
    selectedTab: {
      description: 'Decides which tab should be selected using zero-indexed number.',
      control: 'number',
      defaultValue: 0
    }
  }
};

const DropdownTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsTabsGroup, WsTab },
  template:
`<WsTabsGroup v-bind='$props'>` + 
  `<WsTab title="Tab 1">${args.slotContent}</WsTab>` +
  `<WsTab title="Tab 2">${args.slotContent}</WsTab>` +
  `<WsTab title="Tab 3">${args.slotContent}</WsTab>` +
  `<WsTab title="Tab 4">${args.slotContent}</WsTab>` +
  `<WsTab title="Tab 5">${args.slotContent}</WsTab>` +
`</WsTabsGroup>`
});

export const MaxTabs = DropdownTemplate.bind({});
MaxTabs.args = {
  dropdown: true,
  slotContent:
    `<div>Option 1</div> <div>Option 2</div>`
};