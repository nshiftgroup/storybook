import WsStatus from '../../components/elements/status.vue';

export default {
  title: 'WS Design System/Atoms/Status',
  component: WsStatus,
  parameters: {
    docs: {
      description: {
        component: 'Status label and icons for several models, like orders and shipments'
      }
    }
  },
  argTypes: {
    label: {
      description: 'Status label',
      default: 'Pending'
    },
    status: {
      description: 'One of pending, dispatched, printed, not_printed, error ...',
      control: { type: 'select', options: ['ok', 'partly_ok', 'error', 'locked', 'missing_rate', 'obs', 'on_hold',
        'not_printed', 'pending', 'warning']},
      default: 'pending'
    },
    show_label: {
      description: 'Show or hide label',
      default: true
    }
  }
}


const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsStatus },
  template: '<WsStatus  v-bind="$props" />',
});

export const Dispatched = Template.bind({});
Dispatched.args = {
  status: 'dispatched',
  label: 'Dispatched'
}

export const Printed = Template.bind({});
Printed.args = {
  status: 'printed',
  label: 'Printed'
}

export const Pending = Template.bind({});
Pending.args = {
  status: 'pending',
  label: 'Pending'
}

export const WithoutLabel = Template.bind({});
WithoutLabel.args = {
  status: 'pending',
  label: 'Pending',
  show_label: false
}