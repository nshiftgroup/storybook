import WsCarousel from '../../components/elements/carousel.vue'

export default {
  title: 'WS Design System/Atoms/Carousel',
  component: WsCarousel,
  parameters: {
    docs: {
      description: {
        component: 'Carousel that supports sliding between different cards'
      }
    }
  },
  argTypes: {
    
  }
};


const SlotTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsCarousel },
  template:
`<WsCarousel v-bind='$props'>` + 
  `<template>${args.slot_content}</template>` +
`</WsCarousel>`
});

export const Default = SlotTemplate.bind({});
Default.args = {
  dropdown: true,
  slot_content:
`		<div>Option 1</div> <div>Option 2</div>`
};