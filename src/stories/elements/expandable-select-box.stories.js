import WsExpandableSelectBox from '../../components/molecules/expandable-select-box.vue'

export default {
  title: 'WS Design System/Molecules/Expandable select box',
  component: WsExpandableSelectBox,
  parameters: {
    docs: {
      description: {
        component: 'Expandable, selectable box with title, label and expandable content'
      }
    }
  },
  argTypes: {
    value: {
      description: 'Whether or not the checkbox is selected',
      control: 'boolean',
      defaultValue: false
    },
    title: {
      description: 'Top text to be shown inside box',
      control: 'text',
      defaultValue: 'Title'
    },
    label: {
      description: 'Bottom text to be shown inside box',
      control: 'text',
      defaultValue: 'label'
    },
    expandable: {
      description: 'Whether the box can be expanded or not',
      control: 'boolean',
      defaultValue: false
    }
  }
};

let vModel = false;

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: () => ({ vModel }),
  components: { WsExpandableSelectBox },
  template:
  "<WsExpandableSelectBox v-model='vModel' v-bind='$props' />"
});

const SlotTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: () => ({ vModel }),
  components: { WsExpandableSelectBox },
  template:
`<WsExpandableSelectBox v-bind='$props' v-model="vModel">` + 
  `<template>${args.slotContent}</template>` +
`</WsExpandableSelectBox>`
});

export const Primary = Template.bind({});
Primary.args = {

};

export const Dropdown = SlotTemplate.bind({});
Dropdown.args = {
  expandable: true,
  slotContent:
`		<div>Option 1</div> <div>Option 2</div>`
};