import WsDropdownCustomContent from '../../components/elements/dropdown-custom-content.vue';

// import some icons
import trash from '../assets/icons/common/delete.svg';
import plus from '../assets/icons/common/plus.svg';
import edit from '../assets/icons/common/edit.svg';

export default {
  title: 'WS Design System/atoms/Dropdown with custom container',
  component: WsDropdownCustomContent,
  parameters: {
    docs: {
      description: {
        component: 'A button that toggles a container dropdown that has custom content'
      }
    }
  },
  argTypes: {
    label: {
      description: 'The label that will be shown inside the button',
      control: 'text',
      defaultValue: 'Click me'
    },
    icon: {
      description:
        "Provide the button with an imported icon, e.g. 'import plus from '~/static/svg-icons/common/plus.svg''",
      control: { type: 'select', options: [trash, plus, edit] }
    },
    size: {
      description: 'Defines the size of the button that toggles dropdown container',
      control: { type: 'select', options: ['small', 'large'] }
    }
  }
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsDropdownCustomContent },
  template: "<WsDropdownCustomContent v-bind='$props' />"
});

const DropdownTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsDropdownCustomContent },
  template:
    `<WsDropdownCustomContent v-bind='$props'>` +
    `<template>${args.slot_content}</template>` +
    `</WsDropdownCustomContent>`
});

export const Dropdown = DropdownTemplate.bind({});
Dropdown.args = {
  dropdown: true,
  slot_content: `		<div>Option 1</div> <div>Option 2</div>`
};

export const WithIcon = DropdownTemplate.bind({});
WithIcon.args = {
  label: 'With icon',
  icon: trash,
  slot_content: `		<div>Option 1</div> <div>Option 2</div>`
};

export const Small = DropdownTemplate.bind({});
Small.args = {
  label: 'Small',
  size: 'small',
  slot_content: `		<div>Option 1</div> <div>Option 2</div>`
};
