import WsStepProgressBar from '../../components/elements/step-progress-bar.vue'

export default {
  title: 'WS Design System/Navigation/Steps Progress Bar',
  component: WsStepProgressBar,
  parameters: {
    docs: {
      description: {
        component: 'A progress bar that dynamically creates steps'
      }
    }
  },
  argTypes: {
    steps: {
      description: 'A list of step names',
      control: 'object',
      defaultValue: [
        { title: 'Step 1' },
        { title: 'Step 2' },
        { title: 'Step 3' }
      ]
    },
    selectedStep: {
      description: 'The current step that is selected',
      control: 'number',
      defaultValue: 0
    },
    direction: {
      description: 'The step progress bar can be shown either horizontally or vertically',
      control: { type: 'select', options: ['horizontal', 'vertical']},
    },
    disableClick: {
      description: 'Disable the ability to click on the steps',
      control: 'boolean',
      defaultValue: false
    },
  }
};
const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsStepProgressBar },
  template: '<WsStepProgressBar v-bind="$props" />',
});

export const MaxTabs = Template.bind({});
MaxTabs.args = {
  direction: 'horizontal'
};

export const Vertical = Template.bind({});
Vertical.args = {
  direction: 'vertical'
};

export const DisableClick = Template.bind({});
DisableClick.args = {
  direction: 'horizontal',
  disableClick: true
};