import WSLoadingSpinner from './loading-spinner.vue';

export default {
  title: 'WS Design System/Atoms/Loading Spinner',
  component: WSLoadingSpinner,
  parameters: {
    docs: {
      description: {
        component: 'A generic spinner for webshipper frontend use.',
      },
    },
  },
  argTypes: {
    size: { 
      description: "Size of the spinner",
      control: { type: 'select', options: ['sm', 'xs'] } 
    },
  },
};

const Template = (args, { argTypes }) => ({
    props: Object.keys(argTypes),
    components: { WSLoadingSpinner },
    template: '<WSLoadingSpinner v-bind="$props" />',
});

  export const Medium = Template.bind({});
Medium.args = {
  size: 'sm',
};

export const Small = Template.bind({});
Small.args = {
  size: 'xs',
};