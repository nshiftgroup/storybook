/**
 * ArgTypes proptypes below. Enjoy!
 * sbool
 * snumber
 * srange
 * stext
 * scolor
 * sselect
 * smulti
 **/
import WsFileUploaderButton from '../../components/molecules/file-uploader-button.vue';

export default {
  title: 'WS Design System/Molecules/File uploader button with descriptive text',
  component: WsFileUploaderButton,
  parameters: {
    docs: {
      description: {
        component: 'File uploader button component with descriptive text.'
      }
    }
  },
  argTypes: {
    rawData: {
      description: 'Is the uploaded file a raw data file?',
      control: 'boolean',
      defaultValue: false
    },
    clearImmediately: {
      description: 'Clears dropped file immediately',
      control: 'boolean',
      defaultValue: false
    },
    accept: {
      description: 'What filetypes are accecpted, separated by comma, e.g. "img/jpg, img/png"',
      control: 'text',
      defaultValue: 'image/jpeg,image/png,image/svg+xml,image/bmp,image/gif'
    },
    preview: {
      description: 'Show preview of uploaded file',
      control: 'boolean',
      defaultValue: true
    },
    buttonText: {
      description: 'Text inside the upload button',
      control: 'text',
      defaultValue: 'Select file'
    },
    helpText: {
      description: 'Text to be displayed next to the button when no file is selected',
      control: 'text',
      defaultValue: 'Select or drag and drop an image you want to add'
    },
    hideLabel: {
      description: 'If set to true, there will not be a helping text next to the upload button',
      control: 'boolean',
      defaultValue: false
    },
    hideUploadedFileName: {
      description: 'Hides the name of the uploaded file',
      control: 'boolean',
      defaultValue: false
    }
  }
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsFileUploaderButton },
  template: "<WsFileUploaderButton v-bind='$props' />"
});

export const Default = Template.bind({});
Default.args = {};

export const NoPreview = Template.bind({});
NoPreview.args = {
  preview: false
};

export const NoHelpText = Template.bind({});
NoHelpText.args = {
  hideLabel: true
};
