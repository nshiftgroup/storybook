import WsQuantityButton from '../../components/buttons/quantity-button.vue';

export default {
  title: 'WS Design System/Atoms/Buttons/Quantity button',
  component: WsQuantityButton,
  parameters: {
    docs: {
      description: {
        component: 'A button made for slecting a specific quantity'
      }
    }
  },
  argTypes: {
    value: {
      description: 'Use v-model with this component',
      control: 'number'
    },
    label: {
      description: 'Label to show inside input field',
      control: 'text'
    },
    min: {
      description: 'Set the minimum number that can be input',
      control: 'number'
    },
    max: {
      description: 'Set the maximum number that can be input',
      control: 'number'
    },
    disabled: {
      description: 'Disables user input and changes background',
      control: 'boolean',
      defaultValue: false
    }
  }
};

let vModel = 0;

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  data: () => ({ vModel }),
  components: { WsQuantityButton },
  template: "<WsQuantityButton v-bind='$props' v-model='vModel' />"
});

export const Primary = Template.bind({});
Primary.args = {};

export const WithValue = Template.bind({});
WithValue.args = {
  vModel: 2
};

export const WithLabel = Template.bind({});
WithLabel.args = {
  label: 'This is a label'
}

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true
};
