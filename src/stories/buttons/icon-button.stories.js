import { action } from '@storybook/addon-actions';

import WsIconButton from '../../components/buttons/icon-button.vue';

// import some icons
import trash from '../assets/icons/common/delete.svg';
import print from '../assets/icons/common/print.svg';
import edit from '../assets/icons/common/edit.svg';

export default {
  title: 'WS Design System/Atoms/Buttons/Icon Button',
  component: WsIconButton,
  parameters: {
    docs: {
      description: {
        component: 'A generic icon-button for webshipper frontend use.'
      }
    }
  },
  argTypes: {
    icon: {
      description:
        "Provide the button with an imported icon, e.g. 'import plus from '~/static/svg-icons/common/plus.svg''",
      control: { type: 'select', options: [trash, print, edit] },
      defaultValue: edit
    },
    icon_position: {
      description: 'Positions the icon either to the left or right of text, if there is text',
      control: { type: 'select', options: ['left', 'right'] }
    },
    background_color: {
      description: 'Defines the background color of the button',
      control: 'color'
    },
    size: {
      description: 'Size of the button. Small does NOT work currently',
      control: { type: 'select', options: ['small', 'medium', 'large'] }
    },
    tooltip: {
      description: 'A tooltip that will be displayed when hovering over the button.'
    },
    text: {
      description: 'Text that is displayed inside the button'
    },
    disableIconHovering: {
      description: 'Disables the hovering effect on the icon',
      control: 'boolean',
      defaultValue: false
    },
    disabled: {
      description: 'Disabled input and fades the button',
      control: 'boolean',
      defaultValue: false
    },
    active: {
      description: 'Toggles active color, which looks like hover state',
      control: 'boolean',
      defaultValue: false
    }
  }
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsIconButton },
  template: '<WsIconButton @click="action" v-bind="$props" />',
  methods: { action: action('clicked') }
});

const DropdownTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsIconButton },
  template:
    `<WsIconButton @click="action" v-bind="$props">` + `<template>${args.slot_content}</template>` + `</WsIconButton>`,
  methods: { action: action('clicked') }
});

export const Primary = Template.bind({});
Primary.args = {};

export const Small = Template.bind({});
Small.args = {
  size: 'small'
};

export const Large = Template.bind({});
Large.args = {
  size: 'large'
};

export const WithText = Template.bind({});
WithText.args = {
  text: 'Icon button'
};

export const DisableIconHovering = Template.bind({});
DisableIconHovering.args = {
  text: 'Icon button',
  disableIconHover: true
};

export const WithTextIconRight = Template.bind({});
WithTextIconRight.args = {
  text: 'Icon button',
  icon_position: 'right'
};

export const Disabled = Template.bind({});
Disabled.args = {
  text: 'Disabled button',
  disabled: true
};

export const ConstantlyActive = Template.bind({});
ConstantlyActive.args = {
  text: 'Active button',
  active: true
};

export const Dropdown = DropdownTemplate.bind({});
Dropdown.args = {
  dropdown: true,
  slot_content: '<div>Option 1</div> <div>Option 2</div>'
};
