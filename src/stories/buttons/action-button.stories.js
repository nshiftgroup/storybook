import { action } from '@storybook/addon-actions';

import WsActionButton from '../../components/buttons/action-button.vue';

export default {
  title: 'WS Design System/Atoms/Buttons/Action Button',
  component: WsActionButton,
  parameters: {
    docs: {
      description: {
        component: 'A generic button for webshipper frontend use. Comes in _4 shapes_ and _3 sizes_'
      }
    }
  },
  argTypes: {
    isLoading: {
      description: 'Disables button for clicks and renders a loading animation',
      control: 'boolean',
      defaultValue: false
    },
    label: {
      description: 'Label for the button',
      defaultValue: 'Button'
    },
    primary: {
      description: 'Renders primary coloured and bordered button',
      control: 'boolean',
      defaultValue: false
    },
    danger: {
      description: 'Renders danger coloured and bordered button',
      control: 'boolean',
      defaultValue: false
    },
    plus: {
      description: 'Renders a plus icon inside button',
      control: 'boolean',
      defaultValue: false
    },
    disabled: {
      description: 'Makes it impossible to click button. Will not emit events',
      control: 'boolean',
      defaultValue: false
    },
    hideBorder: {
      description: 'Hides the border of the button - useful for making it not look like a button - e.g. a "link"',
      control: 'boolean',
      defaultValue: false
    },
    minor: {
      description: 'Makes the button white with a border',
      control: 'boolean',
      defaultValue: false
    },
    dropdown: {
      description: 'Toggles whether the button is a dropdown or not',
      control: 'boolean',
      defaultValue: false
    },
    dropdownSplit: {
      description: 'Splits button to have both a onclick and a seperate button to open a dropdown.'
    },
    expandDirection: {
      description:
        'Manually overwrite simulated expand direction where border and radius is removed.' +
        ' Does not care about casing',
      defaultValue: 'bottom',
      control: { type: 'select', options: ['top', 'bottom'] }
    },
    size: {
      description: 'Size of the button. Small does NOT work currently',
      control: { type: 'select', options: ['small', 'medium', 'large'] }
    },
    backgroundColor: {
      description: 'Defines the background color of the button',
      control: 'color',
      table: {
        type: {
          summary: 'something short',
          detail: 'something really really long'
        }
      }
    },
    fullWidth: {
      description: 'Sets the button to be 100% width of parent container',
      control: 'boolean',
      defaultValue: false
    }
  }
};

let asyncClick = async function () {
  return new Promise((resolve) => setTimeout(resolve, 1000));
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsActionButton },
  template: '<WsActionButton @click="action" v-bind="$props" />',
  methods: { action: () => asyncClick().then(() => action('clicked!')) }
});

const WideContainerTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsActionButton },
  template: '<div style="width: 400px;"><WsActionButton @click="action" v-bind="$props" /></div>',
  methods: { action: () => asyncClick().then(() => action('clicked!')) }
});

const DropdownTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { WsActionButton },
  template: `<WsActionButton v-bind="$props">` + `<template>${args.slotContent}</template>` + `</WsActionButton>`
});

export const Primary = Template.bind({});
Primary.args = {
  primary: true,
  label: 'Primary action button'
};

export const Danger = Template.bind({});
Danger.args = {
  danger: true,
  label: 'Danger!'
};

export const Secondary = Template.bind({});
Secondary.args = {
  label: 'Secondary action button'
};
export const Plus = Template.bind({});
Plus.args = {
  plus: true,
  label: 'Button with a plus sign'
};

export const Large = Template.bind({});
Large.args = {
  size: 'large'
};

export const Small = Template.bind({});
Small.args = {
  size: 'small'
};

export const Minor = Template.bind({});
Minor.args = {
  minor: true
};

export const FullWidth = WideContainerTemplate.bind({});
FullWidth.args = {
  fullWidth: true
};

export const Dropdown = DropdownTemplate.bind({});
Dropdown.args = {
  dropdown: true,
  slotContent: '<div>Option 1</div> <div>Option 2</div>'
};

export const DropdownSplit = DropdownTemplate.bind({});
DropdownSplit.args = {
  dropdown: false,
  dropdownSplit: true,
  slotContent: '<div>Option 1</div> <div>Option 2</div>'
};
