import { createLocalVue, shallowMount } from '@vue/test-utils';
import { beforeEach, it } from 'vitest';

// See https://vitest.dev/api/ for documentation
import WsQuantityButton from './quantity-button.vue'

describe('quantity-button.vue', () => {
  const localVue = createLocalVue();
  let component;

  beforeEach(() => {
    // Mount and add mocking to component, e.g. t
    component = shallowMount(WsQuantityButton, {
      localVue,
      propsData: {
        value: null,
        min: null,
        max: null,
        disabled: false
      }
  })})

  it('should render without problems', () => {
    // Assert
    expect(component.vm).toBeTruthy()
  });

  it('should emit if increment() has been run', () => {
    component.vm.increment()
  
    expect(component.emitted().input).toBeTruthy()
  });

  it('should emit if decrement() has been run', () => {
    component.vm.decrement()
  
    expect(component.emitted().input).toBeTruthy()
  });

  it('should increment', async () => {
    await component.setProps({ value: 3 })
    await component.vm.increment()
  
    expect(component.emitted().input[0][0]).toBe(4)
  });

  it('should decrement', async () => {
    await component.setProps({ value: 3 })
    await component.vm.decrement()
  
    expect(component.emitted().input[0][0]).toBe(2)
  });

  it('should not emit if value is over max', async () => {
    await component.setProps({ value: 3, max: 3})
    await component.vm.increment()
  
    expect(component.emitted().input).toBe(undefined)
  });

  it('should not emit if value is below min', async () => {
    await component.setProps({ value: 0, min: 0})
    await component.vm.decrement()
  
    expect(component.emitted().input).toBe(undefined)
  });

  it('should not emit if disabled', async () => {
    await component.setProps({ value: 3, disabled: true})
    await component.vm.decrement()
  
    expect(component.emitted().input).toBe(undefined)

    await component.vm.increment()

    expect(component.emitted().input).toBe(undefined)

  });

});