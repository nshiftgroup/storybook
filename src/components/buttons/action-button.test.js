import { createLocalVue, mount } from '@vue/test-utils';
import { afterEach, beforeEach, it, vi } from 'vitest';
import _ from 'lodash'

// See https://vitest.dev/api/ for documentation
import WsActionButton from './action-button.vue'

describe('action-button.vue', () => {
  const localVue = createLocalVue();
  let component;

  beforeEach(() => {
    // Mount and add mocking to component, e.g. t
    component = mount(WsActionButton, {
      localVue
    });
  })

  it('should render without problems', () => {
    // Assert
    expect(component.vm).toBeTruthy()
  });

  it('should emit "click" on pressed', () => {
    // Act
    component.vm.debouncedClick();
    
    // Assert
    expect(component.emitted().click).toBeTruthy();
  });

});