import { createLocalVue, mount } from '@vue/test-utils';
import { afterEach, beforeEach, it, vi } from 'vitest';

// See https://vitest.dev/api/ for documentation

import WsIconButton from './icon-button.vue'

describe('icon-button.vue', () => {
  const localVue = createLocalVue();

  // If mixins are needed, use code below:
  // localVue.mixin(mixinName)

  // Mount and add mocking to component, e.g. t
  const component = mount(WsIconButton, {
    localVue,
    mocks: {
      t: string => string
    }
  });

  it('should render without problems', () => {
    // Assert
    expect(component.vm).toBeTruthy();
  });

  it('should emit "click" on pressed', () => {
    // Act
    component.vm.debouncedClick();
    
    // Assert
    expect(component.emitted().click).toBeTruthy();
  });

  it('should only be able to click once per second', () => {
    // Act - click 10 times REALLY faaaast
    for (let i = 0; i < 10; i++) {
      component.vm.debouncedClick();
    }
    
    // Assert
    expect(component.emitted().click).toBeTruthy();
    expect(component.emitted().click.length).toBe(1);
  });
});