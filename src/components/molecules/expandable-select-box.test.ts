import { createLocalVue, mount } from '@vue/test-utils';
import { afterEach, beforeEach, it, vi, describe, expect } from 'vitest';

// See https://vitest.dev/api/ for documentation
import WsExpandableSelectBox from './expandable-select-box.vue';
import WsCheckbox from '../input/checkbox.vue';

describe('WsExpandableSelectBox', () => {
  const localVue = createLocalVue();

  // Mount and add mocking to component, e.g. t
  const component = mount(WsExpandableSelectBox, {
    localVue,
  });

  it('should emit "input" when checkbox is toggled', async() => {
    // Arrange
    const checkbox = component.findComponent(WsCheckbox);

    // Act
    checkbox.vm.$emit('input', false);
    await component.vm.$nextTick();

    // Assert
    expect(component.emitted().input).toBeTruthy()
  });

  describe('expandable', () => {
    beforeEach(async() => {
      await component.setProps({
        expandable: true
      })
    });

    it.todo('should have expandable content when expanded', async() => {
      // Act
      await component.vm.setExpandedTo(true);
      
      // Assert
      expect(component.classes()).toContain('selectable-box__expandable-content');
    });


  });
});